package viid

import "strings"

// SubscribeListObject 订阅列表对象
type SubscribeListObject struct {
	SubscribeListObject SubscribeList `json:"SubscribeListObject"`
}

type SubscribeList struct {
	SubscribeObject []Subscribe `json:"SubscribeObject"`
	// TotalNum        int         `json:"TotalNum"`
}

type Subscribe struct {
	SubscribeID           string              `json:"SubscribeID"`                     // R/0 订阅标识符. 该订阅标识符，数据共享接口调用时由 VIID 生成，级联接口调用时不可为空，取消订阅时必选
	Title                 string              `json:"Title"`                           // R/0 订阅标题. 描述订阅的主题和目标，订阅时必选
	SubscribeDetail       SubscribeDetailType `json:"SubscribeDetail"`                 // R/0 订阅类别. 订阅时必选，可同时带多个类别（此时是一个 string），用英文半角逗号分隔
	ResourceURI           string              `json:"ResourceURI"`                     // R/0 订阅资源路. 资源路径 URI(卡口 ID、设备ID、采集内容 ID、案件 ID、目标视图库 ID、行政区编号2/4/6 位等)支持批量和单个订阅，订阅时必选
	ApplicantName         string              `json:"ApplicantName,omitempty"`         // R/0 申请人. 订阅时必选
	ApplicantOrg          string              `json:"ApplicantOrg,omitempty"`          // R/0 申请单位. 申请单位名称，订阅时必选
	BeginTime             DateTime            `json:"BeginTime"`                       // R/0 申请单位名称，订阅时必选
	EndTime               DateTime            `json:"EndTime"`                         // R/0 结束时间, 订阅时必选
	ReceiveAddr           string              `json:"ReceiveAddr"`                     // R/0 信息接收地址, 订阅信息接收地址 URL 如: http://x.x.x.x/receive1, 订阅时必选
	ReportInterval        int                 `json:"ReportInterval,omitempty"`        // 0 信息上报间隔时间 单位为秒（ s）， <=0 表示不限制
	Reason                string              `json:"Reason,omitempty"`                // 0 理由 进行该订阅的理由
	OperateType           int                 `json:"OperateType"`                     // R [必须] 操作类型 0： 订阅； 1：取消订阅
	SubscribeStatus       int                 `json:"SubscribeStatus"`                 // R/0 订阅执行状态 该字段只读 0：订阅中 1: 已取消订阅 2: 订阅到期 9: 未订阅
	SubscribeCancelOrg    string              `json:"SubscribeCancelOrg,omitempty"`    // 0 订阅取消单位 仅在取消订阅时使用
	SubscribeCancelPerson string              `json:"SubscribeCancelPerson,omitempty"` // 0 订阅取消人 仅在取消订阅时使用
	CancelTime            DateTime            `json:"CancelTime,omitempty"`            // 0 取消时间 仅在取消订阅时使用
	CancelReason          string              `json:"CancelReason,omitempty"`          // 0 取消原因 仅在取消订阅时使用
}

func (s *Subscribe) GetResourceDeviceIDs() []DeviceIDType {
	var result []DeviceIDType

	resourceIDs := strings.Split(s.ResourceURI, ",")
	for _, resourceID := range resourceIDs {
		uriSize := len(resourceID)
		switch uriSize {
		case 20:
			object := DeviceIDType(resourceID)

			switch object.TypeCode() {
			case "119", "120", "503":
				result = append(result, object)
			}
		}
	}

	return result
}
