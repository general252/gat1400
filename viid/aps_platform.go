package viid

// APSListObject 采集系统列表对象
type APSListObject struct {
	APSListObject APSList `json:"APSListObject"`
}

type APSList struct {
	APSObject []APS `json:"APSObject"`
	// TotalNum  int   `json:"TotalNum"`
}

// APSObject 采集系统对象
type APSObject struct {
	APSObject APS `json:"APSObject"`
}

type APS struct {
	ApsID    DeviceIDType   `json:"ApsID"`    // R 设备ID
	Name     DeviceNameType `json:"Name"`     // R 名称
	IPAddr   IPAddrType     `json:"IPAddr"`   // R IP地址
	IPV6Addr IPV6AddrType   `json:"IPV6Addr"` // 0 IPv6 地址
	Port     NetPortType    `json:"Port"`     // R 端口号
	IsOnline StatusType     `json:"IsOnline"` // R 是否在线
}

// -------------------------------------------------------------------------------------------------------------------

// APSStatusListObject 采集系统状态列表对象
type APSStatusListObject struct {
	APSStatusListObject APEList `json:"APSStatusListObject"`
}

type APSStatusList struct {
	APEStatusObject []APSStatus `json:"APSStatusObject"`
	// TotalNum        int         `json:"TotalNum"`
}

// APSStatusObject 采集系统状态对象
type APSStatusObject struct {
	APSStatusObject APSStatus `json:"APSStatusObject"`
}

type APSStatus struct {
	ApeID       DeviceIDType `json:"ApeID"`       // R 设备ID
	IsOnline    StatusType   `json:"IsOnline"`    // R 是否在线
	CurrentTime DateTime     `json:"CurrentTime"` // R 当前时间
}
