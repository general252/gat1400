package viid

// APEListObject 采集设备列表对象
type APEListObject struct {
	APEListObject APEList `json:"APEListObject"`
}

type APEList struct {
	APEObject []APE `json:"APEObject"`
	// TotalNum  int   `json:"TotalNum"`
}

// APEObject 采集设备对象
type APEObject struct {
	APEObject APE `json:"APEObject"`
}

type APE struct {
	ApeID DeviceIDType   `json:"ApeID"` // R 设备ID
	Name  DeviceNameType `json:"Name"`  // R 名称
	Model ModelType      `json:"Model"` // R 型号

	IPAddr   IPAddrType   `json:"IPAddr"`   // R IP地址
	IPV6Addr IPV6AddrType `json:"IPV6Addr"` // 0 IPv6 地址
	Port     NetPortType  `json:"Port"`     // R 端口号

	Longitude LongitudeType `json:"Longitude"` // R 经度
	Latitude  LatitudeType  `json:"Latitude"`  // R 纬度

	IsOnline StatusType `json:"IsOnline"` // R 是否在线

	PlaceCode PlaceCodeType `json:"PlaceCode"` // R 安装地点行政区 划代码
	Place     string        `json:"Place"`     // 0 256 位置名 具体到摄像机位置或街道门牌号， 由 (乡镇街道)+ (街路巷)+ (门楼牌号)+(门楼详细地址)构成
	OrgCode   OrgCodeType   `json:"OrgCode"`   // 0 管辖单位代码

	CapDirection     int            `json:"CapDirection"`     // 0 车辆抓拍方向 0：拍车头； 1：拍车尾，兼容无视频卡口信息设备
	MonitorDirection HDirectionType `json:"MonitorDirection"` // 0 监视方向
	MonitorAreaDesc  string         `json:"MonitorAreaDesc"`  // 0 监视区域说明

	OwnerApsID DeviceIDType `json:"OwnerApsID"` // O 所属采集系统 采集设备所接的采集系统设备
	UserId     string       `json:"UserId"`     // O 64 用户帐号 用于支持修改设备登陆帐号
	Password   PasswordType `json:"Password"`   // O 口令 用于支持修改设备登陆口令
}

// -------------------------------------------------------------------------------------------------------------------

// APEStatusListObject 采集设备状态列表对象
type APEStatusListObject struct {
	APEStatusListObject APEList `json:"APEStatusListObject"`
}

type APEStatusList struct {
	APEStatusObject []APEStatus `json:"APEStatusObject"`
	// TotalNum        int         `json:"TotalNum"`
}

// APEStatusObject 采集设备状态对象
type APEStatusObject struct {
	APEStatusObject APEStatus `json:"APEStatusObject"`
}

type APEStatus struct {
	ApeID       DeviceIDType `json:"ApeID"`       // R 设备ID
	IsOnline    StatusType   `json:"IsOnline"`    // R 是否在线
	CurrentTime DateTime     `json:"CurrentTime"` // R 当前时间
}
