package viid

// KeepaliveObject 保活对象
type KeepaliveObject struct {
	KeepaliveObject Keepalive `json:"KeepaliveObject"`
}

type Keepalive struct {
	DeviceID DeviceIDType `json:"DeviceID"` // R 设备 ID 保活设备的 ID，指采集设备、采集系统、 应用平台、分析系统等
}
