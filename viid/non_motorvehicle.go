package viid

// NonMotorVehicleListObject 非机动车列表对象
type NonMotorVehicleListObject struct {
	NonMotorVehicleListObject NonMotorVehicleList `json:"NonMotorVehicleListObject"`
}

type NonMotorVehicleList struct {
	NonMotorVehicleObject []NonMotorVehicle `json:"NonMotorVehicleObject"`
}

// NonMotorVehicleObject 非机动车对象
type NonMotorVehicleObject struct {
	NonMotorVehicleObject NonMotorVehicle `json:"NonMotorVehicleObject"`
}

type NonMotorVehicle struct {
	NonMotorVehicleID ImageCntObjectIdType `json:"NonMotorVehicleID"` // R 车辆标识
	InfoKind          InfoType             `json:"InfoKind"`          // R 信息分类 人工采集还是自动采集
	SourceID          BasicObjectIdType    `json:"SourceID"`          // R 来源标识 来源图像标识
	DeviceID          DeviceIDType         `json:"DeviceID"`          // R/0 设备编码 设备编码, 自动采集必选

	LeftTopX  int `json:"LeftTopX"`  // R/O 左上角 X 坐标 车的轮廓外接矩形在画面中的位置，记录左上角坐标及右下角坐标，自动采集记录时为必选
	LeftTopY  int `json:"LeftTopY"`  // R/O 左上角 Y 坐标
	RightBtmX int `json:"RightBtmX"` // R/0 右下角 X 坐标
	RightBtmY int `json:"RightBtmY"` // R/O 右下角 Y 坐标

	MarkTime      DateTime `json:"MarkTime"`      // 0 位置标记时间 人工采集时有效
	AppearTime    DateTime `json:"AppearTime"`    // 0 车辆出现时间 人工采集时有效
	DisappearTime DateTime `json:"DisappearTime"` // 0 车辆消失时间 人工采集时有效

	HasPlate     Boolean        `json:"HasPlate"`     // R 有无车牌
	PlateClass   PlateClassType `json:"PlateClass"`   // R 号牌种类
	PlateColor   ColorType      `json:"PlateColor"`   // R 车牌颜色
	PlateNo      string         `json:"PlateNo"`      // R 车牌号
	VehicleColor ColorType      `json:"VehicleColor"` // R 车身颜色

	SubImageList SubImageInfoList `json:"SubImageList"` // 0 图像列表 可以包含0个或者多个子图像对象
}
