package viid

// VideoSliceInfoListObject 视频片段列表对象
type VideoSliceInfoListObject struct {
	VideoSliceInfoListObject VideoSliceInfoList `json:"VideoSliceInfoListObject"`
}

type VideoSliceInfoList struct {
	VideoSliceInfoObject []VideoSliceInfo `json:"VideoSliceInfoObject"`
	// TotalNum             int              `json:"TotalNum"`
}

// VideoSliceInfoObject 视频片段对象
type VideoSliceInfoObject struct {
	VideoSliceInfoObject VideoSliceInfo `json:"VideoSliceInfoObject"`
}

type VideoSliceInfo struct {
	VideoID                 BasicObjectIdType    `json:"VideoID"`                 // R/O 视频标识 人工采集写入消息中必填
	InfoKind                InfoType             `json:"InfoKind"`                // R 信息分类 人工采集还是自动采集
	VideoSource             DataSourceType       `json:"VideoSource"`             // R 视频来源
	IsAbstractVideo         Boolean              `json:"IsAbstractVideo"`         // O 摘要视频标志 True=摘要视频， false=原始视频，缺少该字段或缺省为 false
	OriginVideoID           BasicObjectIdType    `json:"OriginVideoID"`           // R/O 原始视频ID 摘要视频对应的原始视频 ID，摘要浓缩视频“原始视频 ID/原始视频 URL”两项必选其一
	OriginVideoURL          string               `json:"OriginVideoURL"`          // R/O 256 原始视频URL
	EventSort               EventType            `json:"EventSort"`               // R/O 事件分类 自动分析事件类型，设备采集必选
	DeviceID                DeviceIDType         `json:"DeviceID"`                // O 设备编码 采集设备（摄像机）的设备编码
	StoragePath             string               `json:"StoragePath"`             // O 256 存储路径 视频文件的存储路径，采用 URI 命名规则
	ThumbnailStoragePath    string               `json:"ThumbnailStoragePath"`    // O 256 缩略图存储路径 视频缩略图文件的存储路径，采用 URI 命名规则
	FileHash                string               `json:"FileHash"`                // O 128 视频文件哈希值 使用 MD5 算法
	FileFormat              VideoFormatType      `json:"FileFormat"`              // R 视频文件格式
	CodedFormat             VideoCodeFormatType  `json:"CodedFormat"`             // R 视频编码格式
	AudioFlag               int                  `json:"AudioFlag"`               // R 音频标志 0:无音频， 1:含音频
	AudioCodedFormat        AudioCodeFormatType  `json:"AudioCodedFormat"`        // O 音频编码格式
	Title                   string               `json:"Title"`                   // R 128 题名 视频资料名称的汉语描述
	TitleNote               string               `json:"TitleNote"`               // O 128 题名补充 题名的补充和备注信息
	SpecialName             string               `json:"SpecialName"`             // O 128 专项名 视频资料所属的专项名称
	Keyword                 KeywordType          `json:"Keyword"`                 // O 关键词 能够表述视频资料主要内容的、具有检索意义的词或词组
	ContentDescription      string               `json:"ContentDescription"`      // R 1024 内容描述 对视频内容的简要描述
	MainCharacter           string               `json:"MainCharacter"`           // O 256 主题人物 描述视频资料内出现的主要人物的中文姓名全称， 当有多个时用英文半角分号” ;” 分隔
	ShotPlaceCode           PlaceCodeType        `json:"ShotPlaceCode"`           // R 拍摄地点行政区划代码
	ShotPlaceFullAdress     PlaceFullAddressType `json:"ShotPlaceFullAdress"`     // R 拍摄地点区划内详细地址
	ShotPlaceLongitude      LongitudeType        `json:"ShotPlaceLongitude"`      // O 拍摄地点经度
	ShotPlacetLatitude      LatitudeType         `json:"ShotPlacetLatitude"`      // O 拍摄地点纬度
	HorizontalShotDirection HorizontalShotType   `json:"HorizontalShotDirection"` // O 水平拍摄方向
	VerticalShotDirection   VerticalShotType     `json:"VerticalShotDirection"`   // O 垂直拍摄方向
	SecurityLevel           SecretLevelType      `json:"SecurityLevel"`           // R/O 密级代码 人工采集需要
	VideoLen                float64              `json:"VideoLen"`                // R 视频长度 单位为秒（ s）
	BeginTime               DateTime             `json:"BeginTime"`               // R 视频开始时间 视频标识开始时间
	EndTime                 DateTime             `json:"EndTime"`                 // R 视频结束时间 视频标识结束时间
	TimeErr                 int                  `json:"TimeErr"`                 // R 时间误差 视频标识时间减去实际北京时间的值，单位为秒（ s）
	Width                   int                  `json:"Width"`                   // R 宽度 水平像素值
	Height                  int                  `json:"Height"`                  // R 高度 垂直像素值
	QualityGrade            QualityGradeType     `json:"QualityGrade"`            // O 质量等级
	CollectorName           NameType             `json:"CollectorName"`           // R/O 采集人 视频资料的采集人姓名或采集系统名称，人工采集必选
	CollectorOrg            OrgType              `json:"CollectorOrg"`            // R/O 采集单位名称 视频资料的采集单位名称，人工采集必选
	CollectorIDType         IDType               `json:"CollectorIDType"`         // O 采集人证件类型
	CollectorID             IdNumberType         `json:"CollectorID"`             // O 采集人证件号码 有效证件号码
	EntryClerk              NameType             `json:"EntryClerk"`              // R/O 入库人 视频资料的入库人姓名或入库系统名称，人工采集必选
	EntryClerkOrg           OrgType              `json:"EntryClerkOrg"`           // R/O 入库单位名称 视频资料的入库单位名称，人工采集必选
	EntryClerkIDType        IDType               `json:"EntryClerkIDType"`        // O 入库人证件类型 视频资料入库人的有效证件类型
	EntryClerkID            IdNumberType         `json:"EntryClerkID"`            // O 入库人证件号码 视频资料入库人的有效证件号码
	EntryTime               DateTime             `json:"EntryTime"`               // R/O 入库时间 视图库自动生成，创建报文中不需要该字段
	VideoProcFlag           int                  `json:"VideoProcFlag"`           // O 视频处理标志 0：未处理， 1：视频经过处理
	FileSize                int64                `json:"FileSize"`                // O 文件大小 视频文件大小，单位 byte
}
