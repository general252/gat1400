package viid

// ImageInfoListObject 图像列表对象
type ImageInfoListObject struct {
	ImageInfoListObject ImageInfoList `json:"ImageInfoListObject"`
}

type ImageInfoList struct {
	ImageInfoObject []ImageInfo `json:"ImageInfoObject"`
}

// ImageInfoObject 图像对象
type ImageInfoObject struct {
	ImageInfoObject ImageInfo `json:"ImageInfoObject"`
}

// -------------------------------------------------------------------------------------------------------------------

// ImageListObject 图像集合对象
type ImageListObject struct {
	Image []Image `json:"Image"`
}

type Image struct {
	ImageInfo           ImageInfo           `json:"ImageInfo"`           // R
	PersonList          PersonList          `json:"PersonList"`          // 0
	FaceList            FaceList            `json:"FaceList"`            // 0
	MotorVehicleList    MotorVehicleList    `json:"MotorVehicleList"`    // 0
	NonMotorVehicleList NonMotorVehicleList `json:"NonMotorVehicleList"` // 0
	Data                string              `json:"Data"`                // 0 Base64Binary
}

// -------------------------------------------------------------------------------------------------------------------

type ImageInfo struct {
	ImageID       BasicObjectIdType `json:"ImageID"`                 // R/0 图像标识
	InfoKind      InfoType          `json:"InfoKind"`                // R 信息分类 人工采集还是自动采集
	ImageSource   DataSourceType    `json:"ImageSource"`             // R 图像来源
	SourceVideoID BasicObjectIdType `json:"SourceVideoID,omitempty"` // O 来源视频标识 如果此图像是视频截图，此字段是来源视频的视频 ID
	OriginImageID BasicObjectIdType `json:"OriginImageID"`           // R/O 原始图像标识 图像增强处理输出图像对应的原始图像 ID，增强处理后图像必选
	EventSort     EventType         `json:"EventSort"`               // R/O 事件分类  自动分析事件类型，设备采集必选

	DeviceID    DeviceIDType `json:"DeviceID,omitempty"`    // O 设备编码 采集设备编码
	StoragePath string       `json:"StoragePath,omitempty"` // O 256 存储路径 图像文件的存储路径，采用 URI 命名规则
	FileHash    string       `json:"FileHash,omitempty"`    // O 128 图像文件哈希值 使用 MD5 算法

	FileFormat         ImageFormatType `json:"FileFormat"`         // R 图像文件格式
	ShotTime           DateTime        `json:"ShotTime"`           // R 拍摄时间
	ContentDescription string          `json:"ContentDescription"` //  R 256 内容描述 对图像内容的简要描述

	Title   string      `json:"Title,"`            // R 128 题名 图像资料名称的汉语描述
	Keyword KeywordType `json:"Keyword,omitempty"` // O 关键词 能够正确表述图像资料主要内容、具有检索意义的词或词组

	ShotPlaceFullAddress PlaceFullAddressType `json:"ShotPlaceFullAddress"` // R 拍摄地点区划内详细地址 具体到街道门牌号，可以由乡镇（街道）名称、街路巷名称、门（楼）牌号、门（楼详细地址）组成
	ShotPlaceLongitude   LongitudeType        `json:"ShotPlaceLongitude"`   // O 拍摄地点经度
	ShotPlaceLatitude    LatitudeType         `json:"ShotPlaceLatitude"`    // O 拍摄地点纬度

	HorizontalShotDirection HorizontalShotType `json:"HorizontalShotDirection,omitempty"` // O 水平拍摄方向
	VerticalShotDirection   VerticalShotType   `json:"VerticalShotDirection,omitempty"`   // O  垂直拍摄方向

	SecurityLevel SecretLevelType `json:"SecurityLevel"` // R 密级代码 自动采集时取值为 5
	Width         int             `json:"Width"`         // R 宽度 水平像素值
	Height        int             `json:"Height"`        // R 高度 垂直像素值

	CollectorName    NameType     `json:"CollectorName"`              // R/O 采集人  图像资料的采集人姓名或采集系统名称，人工采集必选
	CollectorOrg     OrgType      `json:"CollectorOrg"`               // R/O 采集单位名称 图像资料的采集单位名称，人工采集必选
	CollectorIDType  IDType       `json:"CollectorIDType,omitempty"`  // O 采集人证件类型 图像资料采集人的有效证件类型
	CollectorID      IdNumberType `json:"CollectorID,omitempty"`      // O 采集人证件号码 图像资料采集人的有效证件号码
	EntryClerk       NameType     `json:"EntryClerk"`                 // R/O 入库人 图像资料的入库人姓名或入库系统名称，人工采集必选
	EntryClerkOrg    OrgType      `json:"EntryClerkOrg"`              // R/O 入库单位名称 图像资料的入库单位名称，人工采集必选
	EntryClerkIDType IDType       `json:"EntryClerkIDType,omitempty"` // O 入库人证件类型 图像资料入库人的有效证件类型
	EntryClerkID     IdNumberType `json:"EntryClerkID,omitempty"`     // O 入库人证件号码 图像资料入库人的有效证件号码
	EntryTime        DateTime     `json:"EntryTime"`                  // R/O 入库时间 视图库自动生成，创建报文中不需要该字段
	ImageProcFlag    int          `json:"ImageProcFlag"`              // O 图像处理标志 0：未处理， 1：图像经过处理
	FileSize         int          `json:"FileSize,omitempty"`         // O 文件大小 图像文件大小，单位 byte
}

// -------------------------------------------------------------------------------------------------------------------

type SubImageInfoList struct {
	SubImageInfoObject []SubImageInfo `json:"SubImageInfoObject,omitempty"`
}

type SubImageInfo struct {
	ImageID     BasicObjectIdType `json:"ImageId"`            // R 图片ID【String(41)】
	EventSort   EventType         `json:"EventSort"`          // 0 视频图像分析处理事件类型
	DeviceID    DeviceIDType      `json:"DeviceID,omitempty"` // 0 设备编码【String(20)】
	StoragePath string            `json:"StoragePath"`        // R 存储路径
	Type        ImageType         `json:"Type"`               // R 图像类型（02-车牌彩色小图，11-人脸图）
	FileFormat  ImageFormatType   `json:"FileFormat"`         // R 图片格式
	ShotTime    DateTime          `json:"ShotTime"`           // R 拍摄时间【格式(yyyyMMddHHmmss)】
	Width       int               `json:"Width,omitempty"`    // 0 水平像素值
	Height      int               `json:"Height,omitempty"`   // 0 垂直像素值
	Data        string            `json:"Data,omitempty"`     // 0 图片（base64）
}
