package viid

// PersonListObject 人员库对象
type PersonListObject struct {
	PersonListObject PersonList `json:"PersonListObject"`
}

type PersonList struct {
	PersonObject []Person `json:"PersonObject"`
	// TotalNum     int      `json:"TotalNum"`
}

type PersonObject struct {
	PersonObject Person `json:"PersonObject"`
}

type Person struct {
	PersonID ImageCntObjectIdType `json:"PersonID"` // R 人员标识
	InfoKind InfoType             `json:"InfoKind"` // R 信息分类 人工采集还是自动采集
	SourceID BasicObjectIdType    `json:"SourceID"` // R 来源标识 来源图像信息标识
	DeviceID DeviceIDType         `json:"DeviceID"` // R/0 设备编码 设备编码，自动采集必选

	LeftTopX  int `json:"LeftTopX"`  // R/0 左上角 X 坐标 人的轮廓外接矩形在画面中的位置，记录矩形框的左上角坐标及右下角坐标，自动采集记录时为必选
	LeftTopY  int `json:"LeftTopY"`  // R/0 左上角 Y 坐标 人的轮廓外接矩形在画面中的位置，记录矩形框的左上角坐标及右下角坐标，自动采集记录时为必选
	RightBtmX int `json:"RightBtmX"` // R/0 右下角 X 坐标 人的轮廓外接矩形在画面中的位置，记录矩形框的左上角坐标及右下角坐标，自动采集记录时为必选
	RightBtmY int `json:"RightBtmY"` // R/0 右下角 Y 坐标 人的轮廓外接矩形在画面中的位置，记录矩形框的左上角坐标及右下角坐标，自动采集记录时为必选

	LocationMarkTime    DateTime `json:"LocationMarkTime"`    // R/0 位置标记时间 人工采集时有效
	PersonAppearTime    DateTime `json:"PersonAppearTime"`    // R/0 人员出现时间 人工采集时有效
	PersonDisAppearTime DateTime `json:"PersonDisAppearTime"` //   0 人员消失时间 人工采集时有效

	IDType           IDType       `json:"IDType"`           // 0 证件种类
	IDNumber         IdNumberType `json:"IDNumber"`         // 0 有效证件号码
	Name             string       `json:"Name"`             // 0 姓名 人员的中文姓名全称
	UsedName         string       `json:"UsedName"`         // 0 曾用名 曾经在户籍管理部门正式登记注册、人事档案中正式记载的姓氏名称
	Alias            string       `json:"Alias"`            // 0 绰号 使用姓名及曾用名之外的名称
	GenderCode       GenderType   `json:"GenderCode"`       // 0 性别代码
	AgeUpLimit       int          `json:"AgeUpLimit"`       // 0 年龄上限
	AgeLowerLimit    int          `json:"AgeLowerLimit"`    // 0 年龄下限
	AccompanyNumber  int          `json:"AccompanyNumber"`  // 0 同行人数 被标注人的同行人数
	HeightUpLimit    int          `json:"HeightUpLimit"`    // 0 身高上限 人的身高最大可能值，单位为厘米(cm)
	HeightLowerLimit int          `json:"HeightLowerLimit"` // 0 身高下限 人的身高最小可能值，单位为厘米(cm)

	Gesture PostureType      `json:"Gesture"` // 姿态(行走, 奔跑等)
	Status  PersonStatusType `json:"Status"`  // 人的状态(醉酒, 亢奋等)

	UmbrellaColor   ColorType `json:"UmbrellaColor"`   // 伞颜色
	RespiratorColor ColorType `json:"RespiratorColor"` // 口罩颜色
	CapColor        ColorType `json:"CapColor"`        // 帽子颜色
	CoatColor       ColorType `json:"CoatColor"`       // 上衣颜色
	TrousersColor   ColorType `json:"TrousersColor"`   // 裤子颜色
	ShoesColor      ColorType `json:"ShoesColor"`      // 鞋子颜色

	IsDriver             BoolType `json:"IsDriver"`             // R/0 是否驾驶员 0:否 1:是 2:不确定
	IsForeigner          BoolType `json:"IsForeigner"`          // R/0 是否涉外人员 0:否 1:是 2:不确定
	IsSuspectedTerrorist BoolType `json:"IsSuspectedTerrorist"` // R 是否涉恐人员 0:否 1:是 2:不确定
	IsCriminalInvolved   BoolType `json:"IsCriminalInvolved"`   // R 是否涉案人员 0:否 1:是 2:不确定
	IsDetainees          BoolType `json:"IsDetainees"`          // R 是否在押人员 0:否 1:是 2:不确定
	IsVictim             BoolType `json:"IsVictim"`             // R 是否被害人  0:否 1:是 2:不确定
	IsSuspiciousPerson   BoolType `json:"IsSuspiciousPerson"`   // R 是否可疑人  0:否 1:是 2:不确定

	SubImageList SubImageInfoList `json:"SubImageList"` // 图像列表 可以包含 0 个或者多个子图像对象
}
