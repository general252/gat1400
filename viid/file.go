package viid

// FileInfoListObject 文件列表对象
type FileInfoListObject struct {
	FileInfoListObject FileInfoList `json:"FileInfoListObject"`
}

type FileInfoList struct {
	FileInfoObject []FileInfo `json:"FileInfoObject"`
}

// FileInfoObject 文件对象
type FileInfoObject struct {
	FileInfoObject FileInfo `json:"FileInfoObject"`
}

// -------------------------------------------------------------------------------------------------------------------

// FileListObject 文件集合对象
type FileListObject struct {
	File []File `json:"File"` // R
}

type File struct {
	FileInfo            FileInfoObject            `json:"FileInfo"`            // R
	PersonList          PersonListObject          `json:"PersonList"`          // 0
	FaceList            FaceListObject            `json:"FaceList"`            // 0
	MotorVehicleList    MotorVehicleListObject    `json:"MotorVehicleList"`    // 0
	NonMotorVehicleList NonMotorVehicleListObject `json:"NonMotorVehicleList"` // 0
	Data                string                    `json:"Data"`                // 0 Base64Binary
}

// -------------------------------------------------------------------------------------------------------------------

type FileInfo struct {
	FileID        BasicObjectIdType `json:"FileID"`        // R/O 标识符 文件唯一标识符
	InfoKind      InfoType          `json:"InfoKind"`      // R 信息分类 人工采集还是自动采集
	Source        DataSourceType    `json:"Source"`        // R 来源
	FileName      FileNameType      `json:"FileName"`      // R 文件名
	StoragePath   string            `json:"StoragePath"`   // R/O 256 存储路径
	FileHash      string            `json:"FileHash"`      // R 32 文件哈希值 采用 MD5 算法
	FileFormat    string            `json:"FileFormat"`    // R 32 文件格式
	Title         string            `json:"Title"`         // R 128 题名
	SecurityLevel SecretLevelType   `json:"SecurityLevel"` // R/O 密级代码
	SubmiterName  NameType          `json:"SubmiterName"`  // R/O 入库人 入库人姓名，人工采集需填
	SubmiterOrg   OrgType           `json:"SubmiterOrg"`   // R/O 入库人单位 入库人单位名称
	EntryTime     DateTime          `json:"EntryTime"`     // R/O 入库时间 视图库自动生成，创建报文中不需要该字段
	FileSize      int               `json:"FileSize"`      // O 文件大小 图像文件大小，单位 byte
}
