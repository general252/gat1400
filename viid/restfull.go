package viid

const (
	SecurityURLVIID                          = "/VIID"
	SecurityURLVIIDS                         = "/VIID/"
	SecurityURLVIIDRegister                  = "/VIID/System/Register"   // 注册
	SecurityURLVIIDUnRegister                = "/VIID/System/UnRegister" // 注销
	SecurityURLVIIDKeepalive                 = "/VIID/System/Keepalive"  // 保活
	SecurityURLVIIDTime                      = "/VIID/System/Time"       // 校时
	SecurityURLVIIDAPEs                      = "/VIID/APEs"
	SecurityURLVIIDAPSs                      = "/VIID/APSs"
	SecurityURLVIIDTollgates                 = "/VIID/Tollgates"
	SecurityURLVIIDLanes                     = "/VIID/Lanes"
	SecurityURLVIIDVideoSlices               = "/VIID/VideoSlices"
	SecurityURLVIIDVideoSlicesS              = "/VIID/VideoSlices/"
	SecurityURLVIIDImages                    = "/VIID/Images"
	SecurityURLVIIDImagesS                   = "/VIID/Images/"
	SecurityURLVIIDFiles                     = "/VIID/Files"
	SecurityURLVIIDFilesS                    = "/VIID/Files/"
	SecurityURLVIIDPersons                   = "/VIID/Persons"
	SecurityURLVIIDPersonsS                  = "/VIID/Persons/"
	SecurityURLVIIDFaces                     = "/VIID/Faces" // 批量人脸
	SecurityURLVIIDFACESS                    = "/VIID/Faces/"
	SecurityURLVIIDMotorVehicles             = "/VIID/MotorVehicles" // 批量机动车
	SecurityURLVIIDMotorVehiclesS            = "/VIID/MotorVehicles/"
	SecurityURLVIIDNonMotorVehicles          = "/VIID/NonMotorVehicles"
	SecurityURLVIIDNonMotorVehiclesS         = "/VIID/NonMotorVehicles/"
	SecurityURLVIIDThings                    = "/VIID/Things"
	SecurityURLVIIDThingsS                   = "/VIID/Things/"
	SecurityURLVIIDScenes                    = "/VIID/Scenes"
	SecurityURLVIIDScenesS                   = "/VIID/Scenes/"
	SecurityURLVIIDCases                     = "/VIID/Cases"
	SecurityURLVIIDCasesS                    = "/VIID/Cases/"
	SecurityURLVIIDDispositions              = "/VIID/Dispositions"
	SecurityURLVIIDDispositionsS             = "/VIID/Dispositions/"
	SecurityURLVIIDDispositionNotifications  = "/VIID/DispositionNotifications" // 批量告警
	SecurityURLVIIDDispositionNotificationsS = "/VIID/DispositionNotifications/"
	SecurityURLVIIDSubscribes                = "/VIID/Subscribes"
	SecurityURLVIIDSubscribesS               = "/VIID/Subscribes/"
	SecurityURLVIIDSubscribeNotifications    = "/VIID/SubscribeNotifications" // 订阅通知
	SecurityURLVIIDAnalysisRules             = "/VIID/AnalysisRules"
	SecurityURLVIIDAnalysisRulesS            = "/VIID/AnalysisRules/"
	SecurityURLVIIDVideoLabels               = "/VIID/VideoLabels"
	SecurityURLVIIDVideoLabelsS              = "/VIID/VideoLabels/"
)

const (
	SecurityURLPATHVIAS                          = "/VIAS"
	SecurityURLPATHVIASS                         = "/VIAS/"
	SecurityURLPATHVIASREGISTER                  = "/VIAS/System/Register"
	SecurityURLPATHVIASUNREGISTER                = "/VIAS/System/UnRegister"
	SecurityURLPATHVIASKEEPALIVE                 = "/VIAS/System/Keepalive"
	SecurityURLPATHVIASTIME                      = "/VIAS/System/Time"
	SecurityURLPATHVIASTASKS                     = "/VIAS/Tasks"
	SecurityURLPATHVIASTASKSS                    = "/VIAS/Tasks/"
	SecurityURLPATHVIASTASKCONTROLS              = "/VIAS/TaskControls"
	SecurityURLPATHVIASTASKSTATUSES              = "/VIAS/TaskStatuses"
	SecurityURLPATHVIASANALYSISRULES             = "/VIAS/AnalysisRules"
	SecurityURLPATHVIASSYSCAPAANALCAPA           = "/VIAS/SystemCapability/AnalysisCapability"
	SecurityURLPATHVIASSYSCAPAENHANANDRESTORCAPA = "/VIAS/SystemCapability/EnhancementAndRestorationCapability"
	SecurityURLPATHVIASSYSCAPARETRIEVALCAPA      = "/VIAS/SystemCapability/RetrievalCapability"
)

const (
	SecurityURLPATHSINFO         = "/Info"
	SecurityURLPATHSDATA         = "/Data"
	SecurityURLPATHSVIDEOSLICESS = "/VideoSlices/"
	SecurityURLPATHSIMAGES       = "/Images"
	SecurityURLPATHSIMAGESS      = "/Images/"
)

const (
	UserIdentify = "User-Identify"
)
