package viid

// UnRegisterObject 注销对象
type UnRegisterObject struct {
	UnRegisterObject UnRegister `json:"UnRegisterObject"`
}

type UnRegister struct {
	DeviceID DeviceIDType `json:"DeviceID"` // R 设备 ID 注销设备的 ID，指采集设备、采集系统、应用平台、分析系统等
}
