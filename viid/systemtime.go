package viid

// SystemTimeObject 系统时间对象
type SystemTimeObject struct {
	SystemTimeObject SystemTime `json:"SystemTimeObject"`
}

type SystemTime struct {
	VIIDServerID DeviceIDType `json:"VIIDServerID"` // R
	TimeMode     string       `json:"TimeMode"`     // R
	LocalTime    DateTime     `json:"LocalTime"`    // R
	TimeZone     string       `json:"TimeZone"`     // R
}
