package viid

// SubscribeNotificationListObject 订阅通知列表对象
type SubscribeNotificationListObject struct {
	SubscribeNotificationListObject SubscribeNotificationList `json:"SubscribeNotificationListObject"`
}

type SubscribeNotificationList struct {
	SubscribeNotificationObject []SubscribeNotification `json:"SubscribeNotificationObject"`
	PageRecordNum               int                     `json:"PageRecordNum"`
	TotalNum                    int                     `json:"TotalNum"`
}

type SubscribeNotification struct {
	NotificationID BusinessObjectIdType `json:"NotificationID"` // R 通知标识 该订阅通知标识符 65010001000004 20170401120101 00001
	SubscribeID    BusinessObjectIdType `json:"SubscribeID"`    // R 订阅标识 订阅标识 123123123150312312321709695568
	Title          string               `json:"Title"`          // R 订阅标识 描述订阅的主题和目标
	TriggerTime    DateTime             `json:"TriggerTime"`    // R 触发时间 20240306114320

	InfoIDs string `json:"InfoIDs"` // R 信息标识 订阅通知的详细信息(人、车、物、场景)标识集合 FaceObjectList

	FaceObjectList            FaceListObject            `json:"FaceObjectList"`            // 0 人脸信息 人脸信息数据集
	DeviceList                any                       `json:"-"`                         // 0 设备 设备信息数据集
	DeviceStatusList          any                       `json:"-"`                         // 0 设备状态 该通知针对批量订阅方式
	PersonObjectList          PersonListObject          `json:"PersonObjectList"`          // 0 人员信息 人员信息数据集
	MotorVehicleObjectList    MotorVehicleListObject    `json:"MotorVehicleObjectList"`    // 0 机动车信息 机动车(过车)信息数据集
	NonMotorVehicleObjectList NonMotorVehicleListObject `json:"NonMotorVehicleObjectList"` // 0 非机动车信息 非机动车数据集
}

func (tis *SubscribeNotification) GetDeviceIDs() []DeviceIDType {
	var (
		ids    = map[DeviceIDType]bool{}
		values []DeviceIDType
	)

	for _, object := range tis.FaceObjectList.FaceListObject.FaceObject {
		ids[object.DeviceID] = true
	}
	for _, object := range tis.PersonObjectList.PersonListObject.PersonObject {
		ids[object.DeviceID] = true
	}
	for _, object := range tis.MotorVehicleObjectList.MotorVehicleListObject.MotorVehicleObject {
		ids[object.DeviceID] = true
	}
	for _, object := range tis.NonMotorVehicleObjectList.NonMotorVehicleListObject.NonMotorVehicleObject {
		ids[object.DeviceID] = true
	}

	for k, _ := range ids {
		values = append(values, k)
	}
	return values
}
