package viid

import (
	"fmt"
	"strings"
)

// ResponseStatusObject 应答状态对象
type ResponseStatusObject struct {
	ResponseStatusObject ResponseStatus `json:"ResponseStatusObject"`
}

func (tis *ResponseStatusObject) SetResponse(code StatusCode, msg string) {
	tis.ResponseStatusObject.StatusCode = code
	tis.ResponseStatusObject.StatusString = msg
}

func (tis *ResponseStatusObject) Status() string {
	return fmt.Sprintf("%v %v", tis.ResponseStatusObject.StatusCode, tis.ResponseStatusObject.StatusString)
}

func (tis *ResponseStatusObject) IsOK() bool {
	return tis.ResponseStatusObject.StatusCode == StatusCodeOK
}

// ResponseStatusListObject 应答状态列表对象
type ResponseStatusListObject struct {
	ResponseStatusList ResponseStatusList `json:"ResponseStatusList"`
}

func (tis *ResponseStatusListObject) IsOK() bool {
	for _, o := range tis.ResponseStatusList.ResponseStatusObject {
		if o.StatusCode != StatusCodeOK {
			return false
		}
	}

	return true
}
func (tis *ResponseStatusListObject) Status() string {
	var buf []string
	for _, o := range tis.ResponseStatusList.ResponseStatusObject {
		buf = append(buf, fmt.Sprintf("%v %v", o.StatusCode, o.StatusString))
	}
	return strings.Join(buf, "\n")
}

func (tis *ResponseStatusListObject) SetResponse(response ResponseStatus) {
	tis.ResponseStatusList.ResponseStatusObject = append(tis.ResponseStatusList.ResponseStatusObject, response)
}

type ResponseStatusList struct {
	ResponseStatusObject []ResponseStatus `json:"ResponseStatusObject"`
}

type ResponseStatus struct {
	Id           string     `json:"Id"`           // 0
	RequestURL   string     `json:"RequestURL"`   // R
	StatusCode   StatusCode `json:"StatusCode"`   // R
	StatusString string     `json:"StatusString"` // 0
	LocalTime    DateTime   `json:"LocalTime"`    // 0
}
