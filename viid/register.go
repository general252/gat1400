package viid

// RegisterObject 注册对象
type RegisterObject struct {
	RegisterObject Register `json:"RegisterObject"`
}

type Register struct {
	DeviceID DeviceIDType `json:"DeviceID"` // R 设备 ID 注册设备的 ID，指采集设备、采集系统、应用平台、分析系统等
}
