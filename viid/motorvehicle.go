package viid

// MotorVehicleListObject 机动车列表对象
type MotorVehicleListObject struct {
	MotorVehicleListObject MotorVehicleList `json:"MotorVehicleListObject"`
}

type MotorVehicleList struct {
	MotorVehicleObject []MotorVehicle `json:"MotorVehicleObject"`
	// TotalNum           int            `json:"TotalNum"`
}

// MotorVehicleObject 机动车对象
type MotorVehicleObject struct {
	MotorVehicleObject MotorVehicle `json:"MotorVehicleObject"`
}

type MotorVehicle struct {
	MotorVehicleID ImageCntObjectIdType `json:"MotorVehicleID"` // R 车辆标识 车辆全局唯一标识
	InfoKind       InfoType             `json:"InfoKind"`       // R 信息分类 人工采集还是自动采集
	SourceID       BasicObjectIdType    `json:"SourceID"`       // R 来源标识 来源图像标识
	DeviceID       DeviceIDType         `json:"DeviceID"`       // R/O 设备编码 设备编码，自动采集必选

	StorageUrl1 string `json:"StorageUrl1"` // R 256 近景照片 卡口相机所拍照片，自动采集必选，图像访问路径，采用 URI 命名规则
	StorageUrl2 string `json:"StorageUrl2"` // O 256 车牌照片
	StorageUrl3 string `json:"StorageUrl3"` // O 256 远景照片 全景相机所拍照片
	StorageUrl4 string `json:"StorageUrl4"` // O 256 合成图
	StorageUrl5 string `json:"StorageUrl5"` // 0 256 缩略图

	LeftTopX  int `json:"LeftTopX"`  // R/O 左上角 X 坐标 车的轮廓外接矩形在画面中的位置，记录左上角坐标及右下角坐标，自动采集记录时为必选
	LeftTopY  int `json:"LeftTopY"`  // R/O 左上角 Y 坐标
	RightBtmX int `json:"RightBtmX"` // R/0 右下角 X 坐标
	RightBtmY int `json:"RightBtmY"` // R/O 右下角 Y 坐标

	MarkTime      DateTime `json:"MarkTime"`      // 0 位置标记时间 人工采集时有效
	AppearTime    DateTime `json:"AppearTime"`    // 0 车辆出现时间 人工采集时有效
	DisappearTime DateTime `json:"DisappearTime"` // 0 车辆消失时间 人工采集时有效

	HasPlate             Boolean        `json:"HasPlate"`             // R/0 有无车牌
	PlateClass           PlateClassType `json:"PlateClass"`           // R/0 号牌种类
	PlateColor           ColorType      `json:"PlateColor"`           // R/O 车牌颜色 指号牌底色，取 ColorType 中部分值： 黑色，白色，黄色，蓝色，绿色
	PlateNo              PlateNoType    `json:"PlateNo"`              // R/O 车牌号 各类机动车号牌编号车牌全部无法识别的以“无车牌”标识，部分未识别的每个字符以半角‘ -’代替
	PlateReliability     string         `json:"PlateReliability"`     //   0 3  号牌识别可信度  整个号牌号码的识别可信度，以 0～100 数值表示百分比，数值越大可信度越高
	PlateCharReliability string         `json:"PlateCharReliability"` //   0 64 每位号牌号码可信度 号牌号码的识别可信度, 以 0～100 数值表示百分比，数值越大可信度越高。按“字符 1-可信度 1，字符 2-可信度 2”方式排列，中间为英文半角连接线、逗号；例如识别号牌号码为：苏 B12345， 则取值为: ”苏-80,B-90,1-90,2-88,3-90,4-67,5-87”

	Speed   SpeedType `json:"Speed"`   // O 行驶速度 单位千米每小时（ km/h）
	Calling int       `json:"Calling"` // O 打电话状态  0：未打电话； 1：打电话中

	SubImageList SubImageInfoList `json:"SubImageList"` // 图像列表 可以包含 0 个或者多个子图像对象
}
