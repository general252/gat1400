package viid

import (
	"fmt"
)

/*
编码规则A (20)
编码规则 A 由中心编码(8 位) 、 行业编码(2 位) 、 类型编码(3 位) 和序号(7 位) 四个码段共20 位十进制数字字符构成,
即系统编码 = 中心编码 + 行业编码 + 类型编码 + 序号

中心编码: 1、2: 省级编码
		 3、4: 市级编码
		 5、6: 区级编码
		 7、8: 基层接入单位编码
行业编码: 9、10   00 社会治安路面接入 政府机关         包括城市路面、 商业街、 公共区域、 重点区域等
				01 社会治安社区接入 政府机关         包括社区、 楼宇、 网吧等
				02 社会治安内部接入 政府机关         包括公安办公楼、 留置室等
				03 社会治安其他接入 政府机关
				04 交通路面接入     政府机关         包括城市主要干道、 国道、 高速交通状况监视
				05 交通卡口接入     政府机关         包括交叉路口、“电子警察”、 关口、 收费站等
				06 交通内部接入     政府机关         包括交管办公楼等
				07 交通其他接入     政府机关
				08 城市管理接入     政府机关
				09 卫生环保接入     政府机关
				10 商检海关接入     政府机关
				11 教育部门接入     政府机关
				12~39             政府机关         预留
				40 农林牧渔业接入   企业/事业单位
				41 采矿企业接入     企业/事业单位
				42 制造企业接入     企业/事业单位
				43 冶金企业接入     企业/事业单位
				44 电力企业接入     企业/事业单位
				45 燃气企业接入     企业/事业单位
				46 建筑企业接入     企业/事业单位
				47 物流企业接入     企业/事业单位
				48 邮政企业接入     企业/事业单位
				49 信息企业接入     企业/事业单位
				50 住宿和餐饮业接入 企业/事业单位
				51 金融企业接入     企业/事业单位
				52 房地产业接入     企业/事业单位
				53 商务服务业接入   企业/事业单位
				54 水利企业接入     企业/事业单位
				55 娱乐企业接入     企业/事业单位
				56~79               企业/事业单位    预留
				56~79               居民自建         预留
				90~99               其他主体         预留
类型编码:11、12、13  119-在线视频图像信息采集设备
                   120-在线视频图像信息采集系统
                   121-视频卡口
                   502-公安视频图像分析设备/系统
                   503-公安视频图像信息数据库
                   504-公安视频图像信息应用平台
                   131 摄像机编码
                   132 网络摄像机(IPC) 编码
				   111 ~ 130 表示类型为前端主设备
				   131 ~ 199 表示类型为前端外围设备
				   200 ~ 299 表示类型为平台设备
网络标识: 14 网络标识编码 0、1、2、3、4 为监控报警专网,5 为公安信息网,6 为政务网,7 为Internet网,8 为社会资源接入网, 9 预留
序号:    15~20 设备、 用户序号

示例: 34078100 00 119 0 001002
中心编码: https://openstd.samr.gov.cn/bzgk/gb/newGbInfo?hcno=C9C488FD717AFDCD52157F41C3302C6D


视频图像信息基本对象统一标识编码规则 (41)
设备编码/应用平台编码: 1-20   GB/T28181附录D中D.1规定的编码规则
						1.对自动采集对象,应使用在线视频图像信息采集设备/系统、分析设备/系统统一标识编码；
						2.对人工采集对象,应使用对应的公安视频图像信息应用平台或其他公安信息系统统一标识编码，包括所有通过数据服务接口接入视图库的系统
子类型编码: 21-22 表示视频图像信息基本对象的类型 01-视频片段
                                           02-图像
                                           03-文件
                                           99-其他
时间编码: 23-36 表示视频图像信息基本对象生成时间，精确到秒级 YYYYMMDDhhmmss，年月日时分秒
序号: 37-41 视频图像信息基本对象序号

示例: 34078100 00 119 0 001002 03 20240214121314 00001


视频图像信息语义属性对象统一标识编码规则 (48)
视频图像信息基本对象统一标识: 1-41 视频图像信息语义属性对象所属的视频图像信息基本对象 视频图像信息基本对象统一标识
子类型编码: 42-43 表示视频图像信息语义属性对象的类型 01-人员
											  02-机动车
											  03-非机动车
											  04-物品
											  05-场景
											  06-人脸
											  07-视频图像标签
											  99-其他
序号: 44-48 视频图像信息语义属性对象序号


示例: 34078100 00 119 0 001002 03 20240214121314 00001 06 00001
*/

/*

布控与订阅统一标识编码规则 (33)
机构编码：  1-12 公安机关机构代码 采用 GA/T543.1-2011中DE00060
子类型编码：13-14 01-布/撤控
				02-告警
				03-订阅
				04-通知
			    99-其它
时间编码：15-28 表示布控与订阅生成时间，精确到秒级 YYYYMMDDhhmmss，年月日时分秒
序号 29-33 表示流水序号

示例:  650100010000 04 20170401120101 00001

*/

// DeviceIDTypeB20 采集设备、卡口点位、采集系统、分析系统、视图库、应用平台等设备编码规则
type DeviceIDTypeB20 struct {
	Province     string // 省级编码 34 安徽
	City         string // 市级编码 01 合肥
	District     string // 区级编码 04 蜀山区
	BaseUnitCode string // 基层接入单位编码 00

	IndustryCode string // 行业编码 49 信息企业接入

	TypeCode string // 类型编码 119-在线视频图像信息采集设备 120-在线视频图像信息采集系统

	NetworkCode string // 网络标识 0、1、2、3、4 为监控报警专网,5 为公安信息网,6 为政务网,7 为Internet网,8 为社会资源接入网, 9 预留

	DeviceSeq string // 设备、 用户序号  001002
}

func (tis *DeviceIDTypeB20) String() string {
	return fmt.Sprintf("%2s%2s%2s%2s%2s%3s%1s%5s",
		tis.Province, tis.City, tis.District, tis.BaseUnitCode,
		tis.IndustryCode,
		tis.TypeCode,
		tis.NetworkCode,
		tis.DeviceSeq)
}

// BasicObjectIdTypeB41 图像信息基本要素 ID，视频、图像、文件
type BasicObjectIdTypeB41 struct {
	DeviceIDTypeB20

	SubTypeCode string   // 子类型编码 表示视频图像信息基本对象的类型 01-视频片段  02-图像  03-文件  99-其他
	TimeCode    DateTime // 时间编码 表示视频图像信息基本对象生成时间，精确到秒级 YYYYMMDDhhmmss，年月日时分秒 20060102150405
	ObjectSeq   string   // 视频图像信息基本对象序号 00001
}

func (tis *BasicObjectIdTypeB41) String() string {
	b20 := tis.DeviceIDTypeB20.String()
	return fmt.Sprintf("%20s%2s%14s%5s", b20, tis.SubTypeCode, tis.TimeCode, tis.ObjectSeq)
}

// ImageCntObjectIdTypeB48 图像信息内容要素 ID，人、人脸、机动车、非机动车、物品、场景等
type ImageCntObjectIdTypeB48 struct {
	BasicObjectIdTypeB41

	InfoSubTypeCode string // 子类型编码: 42-43 表示视频图像信息语义属性对象的类型 01-人员 02-机动车 03-非机动车 04-物品 05-场景 06-人脸 07-视频图像标签 99-其他
	InfoSeq         string // 视频图像信息语义属性对象序号 00001
}

func (tis *ImageCntObjectIdTypeB48) String() string {
	b41 := tis.BasicObjectIdTypeB41.String()
	return fmt.Sprintf("%41s%2s%5s", b41, tis.InfoSubTypeCode, tis.InfoSeq)
}

// CaseObjectIdTypeB30 string(30) GA/T 1400.1，案(事)件管理对象 ID
type CaseObjectIdTypeB30 struct {
	OrgCode  string   // 机构编码: 1-12 公安机关机构代码 采用 GA/T543.1-2011中DE00060
	TimeCode DateTime // 时间编码: 13-26 表示布控与订阅生成时间，精确到秒级 YYYYMMDDhhmmss，年月日时分秒 20060102150405
	Seq      string   // 序号: 27-30 表示流水序号 0001
}

func (tis *CaseObjectIdTypeB30) String() string {
	return fmt.Sprintf("%12s%14s%4s", tis.OrgCode, tis.TimeCode, tis.Seq)
}

// BusinessObjectIdTypeB33 布控、订阅及相应通知 管理对象 ID 示例:  650100010000 04 20060102150405 00001
type BusinessObjectIdTypeB33 struct {
	OrgCode     string   // 机构编码: 1-12 公安机关机构代码 采用 GA/T543.1-2011中DE00060
	SubTypeCode string   // 子类型编码: 13-14 01-布/撤控 02-告警 03-订阅 04-通知  99-其它
	TimeCode    DateTime // 时间编码: 15-28 表示布控与订阅生成时间，精确到秒级 YYYYMMDDhhmmss，年月日时分秒 20060102150405
	Seq         string   // 序号 29-33 表示流水序号 00001
}

func (tis *BusinessObjectIdTypeB33) String() string {
	return fmt.Sprintf("%12s%2s%14s%5s", tis.OrgCode, tis.SubTypeCode, tis.TimeCode, tis.Seq)
}
