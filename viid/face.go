package viid

// FaceListObject 人脸库列表对象
type FaceListObject struct {
	FaceListObject FaceList `json:"FaceListObject"`
}

type FaceList struct {
	FaceObject []Face `json:"FaceObject,omitempty"`
	// TotalNum   int    `json:"TotalNum"`
}

// FaceObject 人脸库对象
type FaceObject struct {
	FaceObject Face `json:"FaceObject"`
}

type Face struct {
	FaceID   ImageCntObjectIdType `json:"FaceID"`   // R 人脸标识
	InfoKind InfoType             `json:"InfoKind"` // R 信息分类 人工采集还是自动采集
	SourceID BasicObjectIdType    `json:"SourceID"` // R 来源标识 来源图像信息标识
	DeviceID DeviceIDType         `json:"DeviceID"` // R/0 设备编码 设备编码，自动采集必选

	LeftTopX  int `json:"LeftTopX"`  // R/0 左上角 X 坐标 人脸区域，自动采集记录时为必选
	LeftTopY  int `json:"LeftTopY"`  // R/0 左上角 Y 坐标 人脸区域，自动采集记录时为必选
	RightBtmX int `json:"RightBtmX"` // R/0 右下角 X 坐标 人脸区域，自动采集记录时为必选
	RightBtmY int `json:"RightBtmY"` // R/0 右下角 Y 坐标 人脸区域，自动采集记录时为必选

	LocationMarkTime  DateTime `json:"LocationMarkTime"`  // 位置标记时间 人工采集时有效
	FaceAppearTime    DateTime `json:"FaceAppearTime"`    // 人脸出现时间 人工采集时有效
	FaceDisAppearTime DateTime `json:"FaceDisAppearTime"` // 人脸消失时间 人工采集时有效

	IsDriver             BoolType `json:"IsDriver"`             // R/0 是否驾驶员 0:否 1:是 2:不确定
	IsForeigner          BoolType `json:"IsForeigner"`          // R/0 是否涉外人员 0:否 1:是 2:不确定
	IsSuspectedTerrorist BoolType `json:"IsSuspectedTerrorist"` // R 是否涉恐人员 0:否 1:是 2:不确定
	IsCriminalInvolved   BoolType `json:"IsCriminalInvolved"`   // R 是否涉案人员 0:否 1:是 2:不确定
	IsDetainees          BoolType `json:"IsDetainees"`          // R 是否在押人员 0:否 1:是 2:不确定
	IsVictim             BoolType `json:"IsVictim"`             // R 是否被害人  0:否 1:是 2:不确定
	IsSuspiciousPerson   BoolType `json:"IsSuspiciousPerson"`   // R 是否可疑人  0:否 1:是 2:不确定

	SimilarityDegree float64      `json:"Similaritydegree,omitempty"` // 0 相似度 [0.0, 1.0]
	IDType           IDType       `json:"IDType,omitempty"`           // 0 证件类型
	IDNumber         IdNumberType `json:"IDNumber,omitempty"`         // 0 有效证件号码
	Name             string       `json:"Name,omitempty"`             // 0 姓名 人员的中文姓名全称
	UsedName         string       `json:"UsedName,omitempty"`         // 0 曾用名 曾经在户籍管理部门正式登记注册、人事档案中正式记载的姓氏名称
	Alias            string       `json:"Alias,omitempty"`            // 0 绰号 使用姓名及曾用名之外的名称

	GenderCode      GenderType `json:"GenderCode,omitempty"`      // 0 性别代码
	AgeUpLimit      int        `json:"AgeUpLimit,omitempty"`      // 0 年龄上限 最大可能年龄
	AgeLowerLimit   int        `json:"AgeLowerLimit,omitempty"`   // 0 年龄下限 最小可能年龄
	AccompanyNumber int        `json:"AccompanyNumber,omitempty"` // 0 同行人脸数 被标注人脸的同行人脸数

	SubImageList SubImageInfoList `json:"SubImageList"` // 图像列表 可以包含 0 个或者多个子图像对象
}
