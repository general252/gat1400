package gat1400

type Device struct {
	id     string
	online bool
}

func NewDevice(id string) *Device {
	return &Device{
		id: id,
	}
}

func (tis *Device) ID() string {
	return tis.id
}

func (tis *Device) SetOnline(online bool) {
	tis.online = online
}

func (tis *Device) OnKeepalive() {

}

func (tis *Device) IsOnline() bool {
	return true
}
