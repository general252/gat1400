package main

import (
	"gitee.com/general252/gat1400"
	"gitee.com/general252/gat1400/viid"
)

func main() {
	gat1400.NewServer().Serve()
}

func x() {
	var _ = viid.FileListObject{
		FileListObject: viid.FileList{
			File: []viid.FileListItemType{
				{
					FileInfo: viid.FileInfoObject{
						FileInfoObject: viid.FileInfo{
							FileID:        "",
							InfoKind:      0,
							Source:        "",
							FileName:      "",
							StoragePath:   "",
							FileHash:      "",
							FileFormat:    "",
							Title:         "",
							SecurityLevel: "",
							SubmiterName:  "",
							SubmiterOrg:   "",
							EntryTime:     "",
							FileSize:      0,
						},
					},
					PersonList: viid.PersonListObject{
						PersonListObject: viid.PersonList{
							PersonObject: []viid.Person{
								{
									PersonID:             "",
									InfoKind:             0,
									SourceID:             "",
									DeviceID:             "",
									LeftTopX:             0,
									LeftTopY:             0,
									RightBtmX:            0,
									RightBtmY:            0,
									LocationMarkTime:     "",
									PersonAppearTime:     "",
									PersonDisAppearTime:  "",
									IDType:               "",
									IDNumber:             "",
									Name:                 "",
									UsedName:             "",
									Alias:                "",
									GenderCode:           "",
									AgeUpLimit:           0,
									AgeLowerLimit:        0,
									AccompanyNumber:      0,
									HeightUpLimit:        0,
									HeightLowerLimit:     0,
									Gesture:              "",
									Status:               "",
									UmbrellaColor:        "",
									RespiratorColor:      "",
									CapColor:             "",
									CoatColor:            "",
									TrousersColor:        "",
									ShoesColor:           "",
									IsDriver:             0,
									IsForeigner:          0,
									IsSuspectedTerrorist: 0,
									IsCriminalInvolved:   0,
									IsDetainees:          0,
									IsVictim:             0,
									IsSuspiciousPerson:   0,
									SubImageList: viid.SubImageInfoList{
										SubImageInfoObject: []viid.SubImageInfo{
											{
												ImageID:     "",
												EventSort:   0,
												DeviceID:    "",
												StoragePath: "",
												FileFormat:  "",
												ShotTime:    "",
												Width:       0,
												Height:      0,
												Data:        "",
												Type:        "",
											},
										},
									},
								},
							},
						},
					},
					FaceList: viid.FaceListObject{
						FaceListObject: viid.FaceList{
							FaceObject: []viid.Face{
								{
									FaceID:               "",
									InfoKind:             0,
									SourceID:             "",
									DeviceID:             "",
									LeftTopX:             0,
									LeftTopY:             0,
									RightBtmX:            0,
									RightBtmY:            0,
									LocationMarkTime:     "",
									FaceAppearTime:       "",
									FaceDisAppearTime:    "",
									IsDriver:             0,
									IsForeigner:          0,
									IsSuspectedTerrorist: 0,
									IsCriminalInvolved:   0,
									IsDetainees:          0,
									IsVictim:             0,
									IsSuspiciousPerson:   0,
									SimilarityDegree:     0,
									IDType:               "",
									IDNumber:             "",
									Name:                 "",
									UsedName:             "",
									Alias:                "",
									GenderCode:           "",
									AgeUpLimit:           0,
									AgeLowerLimit:        0,
									AccompanyNumber:      0,
									SubImageList: viid.SubImageInfoList{
										SubImageInfoObject: []viid.SubImageInfo{
											{
												ImageID:     "",
												EventSort:   0,
												DeviceID:    "",
												StoragePath: "",
												FileFormat:  "",
												ShotTime:    "",
												Width:       0,
												Height:      0,
												Data:        "",
												Type:        "",
											},
										},
									},
								},
							},
						},
					},
					MotorVehicleList: viid.MotorVehicleListObject{
						MotorVehicleListObject: viid.MotorVehicleList{
							MotorVehicleObject: []viid.MotorVehicle{
								{
									MotorVehicleID:       "",
									InfoKind:             0,
									SourceID:             "",
									DeviceID:             "",
									StorageUrl1:          "",
									StorageUrl2:          "",
									StorageUrl3:          "",
									StorageUrl4:          "",
									StorageUrl5:          "",
									LeftTopX:             0,
									LeftTopY:             0,
									RightBtmX:            0,
									RightBtmY:            0,
									MarkTime:             "",
									AppearTime:           "",
									DisappearTime:        "",
									HasPlate:             "",
									PlateClass:           "",
									PlateColor:           "",
									PlateNo:              "",
									PlateReliability:     "",
									PlateCharReliability: "",
									Speed:                0,
									Calling:              0,
									SubImageList: viid.SubImageInfoList{
										SubImageInfoObject: []viid.SubImageInfo{
											{
												ImageID:     "",
												EventSort:   0,
												DeviceID:    "",
												StoragePath: "",
												FileFormat:  "",
												ShotTime:    "",
												Width:       0,
												Height:      0,
												Data:        "",
												Type:        "",
											},
										},
									},
								},
							},
						},
					},
					NonMotorVehicleList: viid.NonMotorVehicleListObject{
						NonMotorVehicleListObject: viid.NonMotorVehicleList{
							NonMotorVehicleObject: []viid.NonMotorVehicle{
								{
									NonMotorVehicleID: "",
									InfoKind:          0,
									SourceID:          "",
									DeviceID:          "",
									LeftTopX:          0,
									LeftTopY:          0,
									RightBtmX:         0,
									RightBtmY:         0,
									MarkTime:          "",
									AppearTime:        "",
									DisappearTime:     "",
									HasPlate:          "",
									PlateClass:        "",
									PlateColor:        "",
									PlateNo:           "",
									VehicleColor:      "",
									SubImageList: viid.SubImageInfoList{
										SubImageInfoObject: []viid.SubImageInfo{
											{
												ImageID:     "",
												EventSort:   0,
												DeviceID:    "",
												StoragePath: "",
												FileFormat:  "",
												ShotTime:    "",
												Width:       0,
												Height:      0,
												Data:        "",
												Type:        "",
											},
										},
									},
								},
							},
						},
					},
					Data: "",
				},
			},
		},
	}
}
