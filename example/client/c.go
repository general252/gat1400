package main

import (
	"context"
	"encoding/base64"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"gitee.com/general252/gat1400"
	"gitee.com/general252/gat1400/viid"
)

func test(object *viid.FaceListObject) []*string {
	var result []*string
	for i := 0; i < len(object.FaceListObject.FaceObject); i++ {
		faceObject := &object.FaceListObject.FaceObject[i]
		for j := 0; j < len(faceObject.SubImageList.SubImageInfoObject); j++ {
			subInfoObject := &faceObject.SubImageList.SubImageInfoObject[j]
			result = append(result, &subInfoObject.Data)
		}
	}

	return result
}

func testUpdate(result []*string) {
	for i := 0; i < len(result); i++ {
		*result[i] = strings.Repeat(*result[i], 2)
	}
}

func main() {
	if true {
		var object = viid.FaceListObject{
			FaceListObject: viid.FaceList{
				FaceObject: []viid.Face{
					{
						SubImageList: viid.SubImageInfoList{
							SubImageInfoObject: []viid.SubImageInfo{
								{
									ImageID:     "a",
									EventSort:   0,
									DeviceID:    "",
									StoragePath: "",
									Type:        "",
									FileFormat:  "",
									ShotTime:    "",
									Width:       0,
									Height:      0,
									Data:        "aaa",
								},
								{
									ImageID:     "b",
									EventSort:   0,
									DeviceID:    "",
									StoragePath: "",
									Type:        "",
									FileFormat:  "",
									ShotTime:    "",
									Width:       0,
									Height:      0,
									Data:        "bbb",
								},
							},
						},
					},
				},
			},
		}
		result := test(&object)
		testUpdate(result)
		log.Println(len(object.FaceListObject.FaceObject))
	}
	var (
		host     = "192.168.6.67"
		port     = 9780
		deviceId = viid.DeviceIDType("34078100001190001002")
		password = viid.PasswordType("123456")
	)

	cli, err := login(host, port, deviceId, password)
	if err != nil {
		log.Println(err)
		return
	} else {
		log.Println("登录成功")
	}

	go func() {
		for {
			time.Sleep(time.Second * 20)

			if replay, err := cli.Keepalive(&viid.KeepaliveObject{KeepaliveObject: viid.Keepalive{DeviceID: deviceId}}); err != nil {
				log.Println(err)
			} else {
				log.Printf("保活: %v", replay.ResponseStatusObject.StatusCode)
			}
		}
	}()

	systemTime(cli)
	if true {
		addFaces(cli)
	}
	if false {
		addPersons(cli)
	}
	if false {
		addMotorVehicles(cli)
	}
	if false {
		addNonMotorVehicles(cli)
	}
	if false {
		addFiles(cli)
	}

	// cli.Login("12312312315031231232", "123456")

	ctx, _ := signal.NotifyContext(context.TODO(), syscall.SIGHUP, syscall.SIGINT, syscall.SIGALRM)
	<-ctx.Done()
}

func login(host string, port int, deviceId viid.DeviceIDType, password viid.PasswordType) (*gat1400.Client, error) {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	cli := gat1400.NewClient(host, port, deviceId, password)
	reply, err := cli.Register(&viid.RegisterObject{RegisterObject: viid.Register{DeviceID: deviceId}})
	if err != nil {
		return nil, err
	} else if !reply.IsOK() {
		return nil, fmt.Errorf("%v", reply.ResponseStatusObject.StatusString)
	}

	return cli, nil
}

func systemTime(cli *gat1400.Client) {
	reply, err := cli.SystemTime()
	if err != nil {
		log.Println(err)
		return
	} else {
		log.Println(reply)
	}
}

func _getFaces(cli *gat1400.Client) *viid.FaceListObject {
	var (
		file1 = `C:\Users\tony\Desktop\aaa.jpg`
		file2 = `C:\Users\tony\Desktop\ddd.jpg`
	)

	var (
		deviceId  = cli.GetDeviceID()
		now       = viid.NewDateTime(time.Now())
		sourceId1 = fmt.Sprintf("%v02%v00001", deviceId, now)
		sourceId2 = fmt.Sprintf("%v02%v00002", deviceId, now)
		sourceId3 = fmt.Sprintf("%v02%v00003", deviceId, now)
		sourceId4 = fmt.Sprintf("%v02%v00004", deviceId, now)
	)

	imageData1, err := os.ReadFile(file1)
	if err != nil {
		panic(err)
	}
	base64Data1 := base64.StdEncoding.EncodeToString(imageData1)

	imageData2, err := os.ReadFile(file2)
	if err != nil {
		panic(err)
	}
	base64Data2 := base64.StdEncoding.EncodeToString(imageData2)

	param := &viid.FaceListObject{
		FaceListObject: viid.FaceList{
			FaceObject: []viid.Face{
				{
					FaceID:               viid.ImageCntObjectIdType(fmt.Sprintf("%v0600001", sourceId1)),
					InfoKind:             viid.InfoTypeAuto,
					SourceID:             viid.BasicObjectIdType(sourceId1),
					DeviceID:             viid.DeviceIDType(deviceId),
					LeftTopX:             1,
					LeftTopY:             0,
					RightBtmX:            0,
					RightBtmY:            0,
					IsDriver:             0,
					IsForeigner:          0,
					IsSuspectedTerrorist: 0,
					IsCriminalInvolved:   0,
					IsDetainees:          0,
					IsVictim:             0,
					IsSuspiciousPerson:   0,
					SubImageList: viid.SubImageInfoList{
						SubImageInfoObject: []viid.SubImageInfo{
							{
								ImageID:    viid.BasicObjectIdType(sourceId1),
								EventSort:  viid.EventType2,
								DeviceID:   viid.DeviceIDType(deviceId),
								FileFormat: "Jpeg",
								ShotTime:   now,
								Width:      522,
								Height:     425,
								Data:       base64Data1,
								Type:       "11",
							},
							{
								ImageID:    viid.BasicObjectIdType(sourceId2),
								EventSort:  viid.EventType2,
								DeviceID:   viid.DeviceIDType(deviceId),
								FileFormat: "Jpeg",
								ShotTime:   now,
								Width:      575,
								Height:     352,
								Data:       base64Data2,
								Type:       "11",
							},
						},
					},
				},

				{
					FaceID:               viid.ImageCntObjectIdType(fmt.Sprintf("%v0600001", sourceId3)),
					InfoKind:             viid.InfoTypeAuto,
					SourceID:             viid.BasicObjectIdType(sourceId3),
					DeviceID:             viid.DeviceIDType(deviceId),
					LeftTopX:             2,
					LeftTopY:             0,
					RightBtmX:            0,
					RightBtmY:            0,
					IsDriver:             0,
					IsForeigner:          0,
					IsSuspectedTerrorist: 0,
					IsCriminalInvolved:   0,
					IsDetainees:          0,
					IsVictim:             0,
					IsSuspiciousPerson:   0,
					SubImageList: viid.SubImageInfoList{
						SubImageInfoObject: []viid.SubImageInfo{
							{
								ImageID:    viid.BasicObjectIdType(sourceId3),
								EventSort:  viid.EventType2,
								DeviceID:   viid.DeviceIDType(deviceId),
								FileFormat: "Jpeg",
								ShotTime:   now,
								Width:      522,
								Height:     425,
								Data:       base64Data1,
								Type:       "11",
							},
							{
								ImageID:    viid.BasicObjectIdType(sourceId4),
								EventSort:  viid.EventType2,
								DeviceID:   viid.DeviceIDType(deviceId),
								FileFormat: "Jpeg",
								ShotTime:   now,
								Width:      575,
								Height:     352,
								Data:       base64Data2,
								Type:       "11",
							},
						},
					},
				},
			},
		},
	}

	return param
}

func addFaces(cli *gat1400.Client) {

	param := _getFaces(cli)

	reply, err := cli.AddFaces(param)
	if err != nil {
		log.Println(err)
	} else if !reply.IsOK() {
		log.Println(reply)
	} else {
		log.Println("add faces succeeded")
	}
}

func _getPersons(cli *gat1400.Client) *viid.PersonListObject {
	var (
		file1 = `C:\Users\tony\Desktop\aaa.jpg`
		file2 = `C:\Users\tony\Desktop\ddd.jpg`
	)

	var (
		deviceId  = cli.GetDeviceID()
		now       = viid.NewDateTime(time.Now())
		sourceId1 = fmt.Sprintf("%v02%v00001", deviceId, now)
		sourceId2 = fmt.Sprintf("%v02%v00002", deviceId, now)
	)

	imageData1, err := os.ReadFile(file1)
	if err != nil {
		panic(err)
	}
	base64Data1 := base64.StdEncoding.EncodeToString(imageData1)

	imageData2, err := os.ReadFile(file2)
	if err != nil {
		panic(err)
	}
	base64Data2 := base64.StdEncoding.EncodeToString(imageData2)

	param := &viid.PersonListObject{
		PersonListObject: viid.PersonList{
			PersonObject: []viid.Person{
				{
					PersonID:             viid.ImageCntObjectIdType(fmt.Sprintf("%v0600001", sourceId1)),
					InfoKind:             viid.InfoTypeAuto,
					SourceID:             viid.BasicObjectIdType(sourceId1),
					DeviceID:             viid.DeviceIDType(deviceId),
					LeftTopX:             0,
					LeftTopY:             0,
					RightBtmX:            0,
					RightBtmY:            0,
					IsDriver:             0,
					IsForeigner:          0,
					IsSuspectedTerrorist: 0,
					IsCriminalInvolved:   0,
					IsDetainees:          0,
					IsVictim:             0,
					IsSuspiciousPerson:   0,
					SubImageList: viid.SubImageInfoList{
						SubImageInfoObject: []viid.SubImageInfo{
							{
								ImageID:     viid.BasicObjectIdType(sourceId1),
								EventSort:   2,
								DeviceID:    viid.DeviceIDType(deviceId),
								StoragePath: "/a/ccc",
								FileFormat:  "Jpeg",
								ShotTime:    now,
								Width:       522,
								Height:      425,
								Data:        base64Data1,
								Type:        "11",
							},
							{
								ImageID:     viid.BasicObjectIdType(sourceId2),
								EventSort:   2,
								DeviceID:    viid.DeviceIDType(deviceId),
								StoragePath: "/a/ddd",
								FileFormat:  "Jpeg",
								ShotTime:    now,
								Width:       575,
								Height:      352,
								Data:        base64Data2,
								Type:        "11",
							},
						},
					},
				},
			},
		},
	}

	return param
}

func addPersons(cli *gat1400.Client) {

	param := _getPersons(cli)

	reply, err := cli.AddPerson(param)
	if err != nil {
		log.Println(err)
	} else if !reply.IsOK() {
		log.Println(reply)
	} else {
		log.Println("add person succeeded")
	}
}

func _getMotorVehicles(cli *gat1400.Client) *viid.MotorVehicleListObject {
	var (
		file1 = `C:\Users\tony\Desktop\aaa.jpg`
	)

	var (
		deviceId  = cli.GetDeviceID()
		now       = viid.NewDateTime(time.Now())
		sourceId1 = fmt.Sprintf("%v02%v00001", deviceId, now)
	)

	imageData1, err := os.ReadFile(file1)
	if err != nil {
		panic(err)
	}

	base64Data1 := base64.StdEncoding.EncodeToString(imageData1)

	param := &viid.MotorVehicleListObject{
		MotorVehicleListObject: viid.MotorVehicleList{
			MotorVehicleObject: []viid.MotorVehicle{
				{
					MotorVehicleID: viid.ImageCntObjectIdType(fmt.Sprintf("%v0600001", sourceId1)),
					InfoKind:       0,
					SourceID:       viid.BasicObjectIdType(sourceId1),
					DeviceID:       "",
					StorageUrl1:    "/a/b/c",
					SubImageList: viid.SubImageInfoList{
						SubImageInfoObject: []viid.SubImageInfo{
							{
								ImageID:     viid.BasicObjectIdType(sourceId1),
								EventSort:   2,
								DeviceID:    viid.DeviceIDType(deviceId),
								StoragePath: "/a/ccc/ddd",
								FileFormat:  "Jpeg",
								ShotTime:    now,
								Width:       522,
								Height:      425,
								Data:        base64Data1,
								Type:        "11",
							},
						},
					},
				},
			},
		},
	}

	return param
}

func addMotorVehicles(cli *gat1400.Client) {

	param := _getMotorVehicles(cli)

	reply, err := cli.AddMotorVehicle(param)
	if err != nil {
		log.Println(err)
	} else if !reply.IsOK() {
		log.Println(reply)
	} else {
		log.Println("add motor vehicle succeeded")
	}
}

func _getNonMotorVehicles(cli *gat1400.Client) *viid.NonMotorVehicleListObject {
	var (
		file1 = `C:\Users\tony\Desktop\aaa.jpg`
	)

	var (
		deviceId  = cli.GetDeviceID()
		now       = viid.NewDateTime(time.Now())
		sourceId1 = fmt.Sprintf("%v02%v00001", deviceId, now)
	)

	imageData1, err := os.ReadFile(file1)
	if err != nil {
		panic(err)
	}

	base64Data1 := base64.StdEncoding.EncodeToString(imageData1)

	param := &viid.NonMotorVehicleListObject{
		NonMotorVehicleListObject: viid.NonMotorVehicleList{
			NonMotorVehicleObject: []viid.NonMotorVehicle{
				{
					NonMotorVehicleID: viid.ImageCntObjectIdType(fmt.Sprintf("%v0600001", sourceId1)),
					InfoKind:          0,
					SourceID:          viid.BasicObjectIdType(sourceId1),
					DeviceID:          "",
					SubImageList: viid.SubImageInfoList{
						SubImageInfoObject: []viid.SubImageInfo{
							{
								ImageID:     viid.BasicObjectIdType(sourceId1),
								EventSort:   2,
								DeviceID:    viid.DeviceIDType(deviceId),
								StoragePath: "/a/ccc/eee",
								FileFormat:  "Jpeg",
								ShotTime:    now,
								Width:       522,
								Height:      425,
								Data:        base64Data1,
								Type:        "11",
							},
						},
					},
				},
			},
		},
	}

	return param
}

func addNonMotorVehicles(cli *gat1400.Client) {
	param := _getNonMotorVehicles(cli)

	reply, err := cli.AddNonMotorVehicle(param)
	if err != nil {
		log.Println(err)
	} else if !reply.IsOK() {
		log.Println(reply)
	} else {
		log.Println("add non motor vehicle succeeded")
	}
}

func addFiles(cli *gat1400.Client) {
	var (
		file1 = `C:\Users\tony\Desktop\aaa.jpg`
	)

	var (
		deviceId  = cli.GetDeviceID()
		now       = viid.NewDateTime(time.Now())
		sourceId1 = fmt.Sprintf("%v02%v00001", deviceId, now)
	)

	imageData1, err := os.ReadFile(file1)
	if err != nil {
		log.Println(err)
		return
	}

	base64Data1 := base64.StdEncoding.EncodeToString(imageData1)

	faceList := _getFaces(cli)
	personList := _getPersons(cli)

	param := &viid.FileListObject{
		File: []viid.File{
			{
				FileInfo: viid.FileInfoObject{
					FileInfoObject: viid.FileInfo{
						FileID:        viid.BasicObjectIdType(sourceId1),
						InfoKind:      viid.InfoTypeAuto,
						Source:        viid.DataSourceType8,
						FileName:      "aaa",
						StoragePath:   "",
						FileHash:      "a",
						FileFormat:    string(viid.ImageFormatTypeJpeg),
						Title:         "xx",
						SecurityLevel: "",
						SubmiterName:  "",
						SubmiterOrg:   "",
						EntryTime:     "",
						FileSize:      0,
					},
				},
				PersonList:          *personList,
				FaceList:            *faceList,
				MotorVehicleList:    viid.MotorVehicleListObject{},
				NonMotorVehicleList: viid.NonMotorVehicleListObject{},
				Data:                base64Data1,
			},
		},
	}

	reply, err := cli.AddFiles(param)
	if err != nil {
		log.Println(err)
	} else if !reply.IsOK() {
		log.Println(reply)
	} else {
		log.Println("add non motor vehicle succeeded")
	}
}
